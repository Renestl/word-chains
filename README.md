# Word Chains

Implementation in Ruby for [App Academy Open](https://open.appacademy.io/)

[Word Chains RubyQuiz](http://rubyquiz.com/quiz44.html)

## Goals:

Given two words of equal length, build a chain of words connecting the first to the second. Each word in the chain must be in the dictionary and every step along the chain changes exactly one letter from the previous word.