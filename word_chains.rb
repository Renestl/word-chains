require "set"

class WordChainer
	attr_reader :dictionary

	def initialize(dictionary)
		words = File.readlines(dictionary).map(&:chomp)
		@dictionary = Set.new(words)
		@all_seen_words = Hash.new
	end

	def adjacent_words(word)
		alphabet = ('a'..'z').to_a
		adjacents = []
		
		(0...word.length).each do |i|
			alphabet.each do |char|
				new_word = word.dup
				new_word[i] = char
				if @dictionary.include?(new_word) && word != new_word
					adjacents << new_word
				end
			end
		end	

		adjacents
	end

	def run(source, target)
		@current_words = [source]
		@all_seen_words[source] = nil

		until @current_words.empty? || @all_seen_words.include?(target)
			explore_current_words
		end

		build_path(target)
	end

	def explore_current_words
		new_current_words = []

		@current_words.each do |current_word|
			adjacents = adjacent_words(current_word)
			adjacents.each do |adjacent_word|
				if !@all_seen_words.include?(adjacent_word)
					new_current_words << adjacent_word
					@all_seen_words[adjacent_word] = current_word 
				end
			end
		end

		new_current_words.map do |word|
			"#{word} came from #{@all_seen_words[word]}"
		end

		@current_words = new_current_words
	end

	def build_path(target)
		path = [target]

		until path.last == nil
			path << @all_seen_words[path.last]
		end

		path.delete(nil)
		path.reverse!
	end
	
end

if $PROGRAM_NAME == __FILE__
	p WordChainer.new('dictionary.txt').run("duck", "ruby")
end